FROM docker:18.09.2-dind

LABEL maintainer="Anatoly Kudriashov <kav2k13@gmail.com>"

RUN apk add --no-cache python py-pip \
    && pip install --no-cache-dir docker-compose && rm -rf /tmp/* \
    && wget https://releases.hashicorp.com/consul-template/0.20.0/consul-template_0.20.0_linux_amd64.tgz \
    && tar -zxf consul-template_0.20.0_linux_amd64.tgz -C /usr/bin/ \
    && rm consul-template_0.20.0_linux_amd64.tgz
COPY config.json /root/.docker/

