# Docker in Docker
Для деплоя Docker контейнеров внутри Docker контейнера в Kubernetes.

- **dind image**  
Используется официальный образ `docker:**-dind`, дополнительно установлен docker-compose, consul-template.  
В образ копируется конфигурационный файл docker c авторизационными данными к приватному регистру
```bash
git clone https://gitlab.com/KudryashovAV/dind.git
cd dind/
#Измените версию если это необходимо
nano Dockerfile
#Cобирите образ
docker build -t avkudryashov/dind:18.06 -t avkudryashov/dind:latest .
#Далее необходимо запушить образ, предварительно авторизовавшись
docker push avkudryashov/dind:18.06
```
- **Gitlab-Runner**  
KUBERNETES_PRIVILEGED: "true" - должно быть выставлено в *true*

![alt text](image.png)